import 'source-map-support/register';

import { APIGatewayProxyHandler } from 'aws-lambda';

import {
  ServerlessJsonApi,
  IAppRouteConfig,
  HttpMethods,
  BaseHandler,
  EndpointTypes
} from '@vifros/serverless-json-api';

import { InfoHandler }  from './handlers/info';
import { UsersHandler } from './handlers/users';
import { PostsHandler } from './handlers/posts';
import { TagsHandler }  from './handlers/tags';

import { userModelLoader } from './models/user';
import { postModelLoader } from './models/post';
import { infoModelLoader } from './models/info';
import { tagModelLoader }  from './models/tag';

export * from './handlers/standalone';

const serverlessJsonApi: ServerlessJsonApi = new ServerlessJsonApi(
  new Map<string, typeof BaseHandler>([
    ['usersHandler', UsersHandler],
    ['infoHandler', InfoHandler],
    ['postsHandler', PostsHandler],
    ['tagsHandler', TagsHandler]
  ]),
  new Set([
    userModelLoader,
    infoModelLoader,
    postModelLoader,
    tagModelLoader
  ]));

const usersHandler = serverlessJsonApi.getHandler('usersHandler') as UsersHandler;
const infoHandler  = serverlessJsonApi.getHandler('infoHandler') as InfoHandler;
const postsHandler = serverlessJsonApi.getHandler('postsHandler') as PostsHandler;
const tagsHandler  = serverlessJsonApi.getHandler('tagsHandler') as TagsHandler;

serverlessJsonApi.registerRoutes(new Set<IAppRouteConfig>([
  //
  // Info.
  //
  {
    method        : HttpMethods.GET,
    route         : '/v1/info',
    handler       : infoHandler.getAllInfo.bind(infoHandler),
    endpointType  : EndpointTypes.Collection,
    relatedModelId: infoModelLoader.id
  },
  {
    method        : HttpMethods.GET,
    route         : '/v1/info/:infoId',
    handler       : infoHandler.getInfoById.bind(infoHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: infoModelLoader.id
  },
  //
  // Users.
  //
  {
    method        : HttpMethods.POST,
    route         : '/v1/users',
    handler       : usersHandler.createUser.bind(usersHandler),
    endpointType  : EndpointTypes.Collection,
    relatedModelId: userModelLoader.id
  },
  {
    method        : HttpMethods.GET,
    route         : '/v1/users',
    handler       : usersHandler.getAllUsers.bind(usersHandler),
    endpointType  : EndpointTypes.Collection,
    relatedModelId: userModelLoader.id
  },
  {
    method        : HttpMethods.GET,
    route         : '/v1/users/:userId',
    handler       : usersHandler.getUserById.bind(usersHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: userModelLoader.id
  },
  {
    method        : HttpMethods.DELETE,
    route         : '/v1/users/:userId',
    handler       : usersHandler.deleteUserById.bind(usersHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: userModelLoader.id
  },
  {
    method        : HttpMethods.PATCH,
    route         : '/v1/users/:userId',
    handler       : usersHandler.updateUserById.bind(usersHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: userModelLoader.id
  },
  //
  // User's Posts.
  //
  {
    method        : HttpMethods.POST,
    route         : '/v1/users/:userId/posts',
    handler       : usersHandler.createUserPost.bind(usersHandler),
    endpointType  : EndpointTypes.Collection,
    relatedModelId: postModelLoader.id
  },
  {
    method        : HttpMethods.GET,
    route         : '/v1/users/:userId/posts',
    handler       : usersHandler.getAllUserPosts.bind(usersHandler),
    endpointType  : EndpointTypes.Collection,
    relatedModelId: postModelLoader.id
  },
  {
    method        : HttpMethods.GET,
    route         : '/v1/users/:userId/posts/:postId',
    handler       : usersHandler.getUserPostById.bind(usersHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: postModelLoader.id
  },
  {
    method        : HttpMethods.PATCH,
    route         : '/v1/users/:userId/posts/:postId',
    handler       : usersHandler.updateUserPostById.bind(usersHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: postModelLoader.id
  },
  {
    method        : HttpMethods.DELETE,
    route         : '/v1/users/:userId/posts/:postId',
    handler       : usersHandler.deleteUserPostById.bind(usersHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: postModelLoader.id
  },
  //
  // Posts.
  //
  {
    method        : HttpMethods.POST,
    route         : '/v1/posts',
    handler       : postsHandler.createPost.bind(postsHandler),
    endpointType  : EndpointTypes.Collection,
    relatedModelId: postModelLoader.id
  },
  {
    method        : HttpMethods.GET,
    route         : '/v1/posts',
    handler       : postsHandler.getAllPosts.bind(postsHandler),
    endpointType  : EndpointTypes.Collection,
    relatedModelId: postModelLoader.id
  },
  {
    method        : HttpMethods.GET,
    route         : '/v1/posts/:postId',
    handler       : postsHandler.getPostById.bind(postsHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: postModelLoader.id
  },
  {
    method        : HttpMethods.DELETE,
    route         : '/v1/posts/:postId',
    handler       : postsHandler.deletePostById.bind(postsHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: postModelLoader.id
  },
  {
    method        : HttpMethods.PATCH,
    route         : '/v1/posts/:postId',
    handler       : postsHandler.updatePostById.bind(postsHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: postModelLoader.id
  },
  //
  // Tags.
  //
  {
    method        : HttpMethods.POST,
    route         : '/v1/tags',
    handler       : tagsHandler.createTag.bind(tagsHandler),
    endpointType  : EndpointTypes.Collection,
    relatedModelId: tagModelLoader.id
  },
  {
    method        : HttpMethods.GET,
    route         : '/v1/tags',
    handler       : tagsHandler.getAllTags.bind(tagsHandler),
    endpointType  : EndpointTypes.Collection,
    relatedModelId: tagModelLoader.id
  },
  {
    method        : HttpMethods.GET,
    route         : '/v1/tags/:tagId',
    handler       : tagsHandler.getTagById.bind(tagsHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: tagModelLoader.id
  },
  {
    method        : HttpMethods.DELETE,
    route         : '/v1/tags/:tagId',
    handler       : tagsHandler.deleteTagById.bind(tagsHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: tagModelLoader.id
  },
  {
    method        : HttpMethods.PATCH,
    route         : '/v1/tags/:tagId',
    handler       : tagsHandler.updateTagById.bind(tagsHandler),
    endpointType  : EndpointTypes.Resource,
    relatedModelId: tagModelLoader.id
  }
]));

export const index: APIGatewayProxyHandler | any = serverlessJsonApi.mainHandler;
