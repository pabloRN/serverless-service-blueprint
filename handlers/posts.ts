import {
  BaseHandler,
  JsonApiNotFoundError
} from '@vifros/serverless-json-api';

import { postModelLoader } from '../models/post';

export class PostsHandler extends BaseHandler {
  constructor() {
    super();
  }

  async createPost(data: any): Promise<any> {
    const Post = this.getModelByLoader(postModelLoader);
    return await Post.xCreate(data);
  }

  async getPostById(postId: string): Promise<any> {
    const Post = this.getModelByLoader(postModelLoader);
    let post   = await Post.xGet({
      id: postId
    });

    if (!post) {
      throw new JsonApiNotFoundError({
        parameter: 'postId'
      });
    }

    return post;
  }

  async deletePostById(postId: string): Promise<void> {
    // TODO @diosney: Add support for a soft delete.
    const Post = this.getModelByLoader(postModelLoader);

    await Post.xDelete({
      id: postId
    });
  }

  async updatePostById(postId: string, postData: any): Promise<any> {
    const Post = this.getModelByLoader(postModelLoader);
    return await Post.xUpdate({
      id: postId
    }, postData);
  }

  async getAllPosts(query?: any): Promise<any> {
    const Post = this.getModelByLoader(postModelLoader);
    return await Post.xGetAll(query);
  }
}
