import {
  BaseHandler,
  ErrorMessages,
  ErrorTitles,
  IAppHandlerMeta,
  JsonApi,
  JsonApiNotFoundError,
  JsonApiValidationError
} from '@vifros/serverless-json-api';

import { userModelLoader } from '../models/user';
import { postModelLoader } from '../models/post';

export class UsersHandler extends BaseHandler {
  constructor() {
    super();
  }

  async createUser(data: any): Promise<any> {
    const User = this.getModelByLoader(userModelLoader);
    return await User.xCreate(data);
  }

  async getUserById(userId: string): Promise<any> {
    const User = this.getModelByLoader(userModelLoader);
    let user   = await User.xGet({
      id: userId
    });

    if (!user) {
      throw new JsonApiNotFoundError({
        parameter: 'userId'
      });
    }

    return user;
  }

  async deleteUserById(userId: string): Promise<void> {
    // TODO @diosney: Add support for a soft delete.
    const User = this.getModelByLoader(userModelLoader);

    await User.xDelete({
      id: userId
    });
  }

  async updateUserById(userId: string, userData: any): Promise<any> {
    const User = this.getModelByLoader(userModelLoader);
    return await User.xUpdate({
      id: userId
    }, userData);
  }

  async getAllUsers(query?: any): Promise<any> {
    const User = this.getModelByLoader(userModelLoader);
    return await User.xGetAll(query);
  }

  async createUserPost(authorId: string, data: any, meta: IAppHandlerMeta): Promise<any> {
    if (meta.isFirstInChain && data.authorId) {
      throw new JsonApiValidationError(ErrorTitles.InvalidAttribute, ErrorMessages.NotAllowedAttribute.replace('{attribute}', 'authorId'));
    }

    const Post    = this.getModelByLoader(postModelLoader);
    data.authorId = authorId;

    let post = await Post.xCreate(data);

    // TODO @diosney: Set this as a property in the handler's registration?
    JsonApi.addExtras(post, {
      isNested: true
    });

    return post;
  }

  async getAllUserPosts(authorId: string, query: any = {}): Promise<any> {
    const User = this.getModelByLoader(userModelLoader);
    const Post = this.getModelByLoader(postModelLoader);

    let user = await User.xGet({
      id: authorId
    });

    if (!user) {
      throw new JsonApiNotFoundError({
        parameter: 'authorId'
      });
    }

    if (!query.filter) {
      query.filter = {};
    }

    query.filter.authorId = {
      $eq: authorId
    };

    return await Post.xGetAll(query);
  }

  async getUserPostById(authorId: string, postId: string): Promise<any> {
    const User = this.getModelByLoader(userModelLoader);
    const Post = this.getModelByLoader(postModelLoader);

    let user = await User.xGet({
      id: authorId
    });

    if (!user) {
      throw new JsonApiNotFoundError({
        parameter: 'authorId'
      });
    }

    let post = await Post.xGet({
      id: postId
    });

    if (!post) {
      throw new JsonApiNotFoundError({
        parameter: 'postId'
      });
    }

    return post;
  }

  async updateUserPostById(authorId: string, postId: string, postData: any, meta: IAppHandlerMeta): Promise<any> {
    const User = this.getModelByLoader(userModelLoader);
    const Post = this.getModelByLoader(postModelLoader);

    if (meta.isFirstInChain && postData.authorId) {
      throw new JsonApiValidationError(ErrorTitles.InvalidAttribute, ErrorMessages.NotAllowedAttribute.replace('{attribute}', 'authorId'));
    }

    let user = await User.xGet({
      id: authorId
    });

    if (!user) {
      throw new JsonApiNotFoundError({
        parameter: 'authorId'
      });
    }

    return await Post.xUpdate({
      id: postId
    }, postData);
  }

  async deleteUserPostById(authorId: string, postId: string): Promise<void> {
    // TODO @diosney: Add support for a soft delete.
    const User = this.getModelByLoader(userModelLoader);
    const Post = this.getModelByLoader(postModelLoader);

    let user = await User.xGet({
      id: authorId
    });

    if (!user) {
      throw new JsonApiNotFoundError({
        parameter: 'authorId'
      });
    }

    await Post.xDelete({
      id: postId
    });
  }
}
