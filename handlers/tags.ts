import {
  BaseHandler,
  JsonApiNotFoundError
} from '@vifros/serverless-json-api';

import { tagModelLoader } from '../models/tag';

export class TagsHandler extends BaseHandler {
  constructor() {
    super();
  }

  async createTag(data: any): Promise<any> {
    const Tag = this.getModelByLoader(tagModelLoader);
    return await Tag.xCreate(data);
  }

  async getTagById(tagId: string): Promise<any> {
    const Tag = this.getModelByLoader(tagModelLoader);
    let tag   = await Tag.xGet({
      id: tagId
    });

    if (!tag) {
      throw new JsonApiNotFoundError({
        parameter: 'tagId'
      });
    }

    return tag;
  }

  async deleteTagById(tagId: string): Promise<void> {
    // TODO @diosney: Add support for a soft delete.
    const Tag = this.getModelByLoader(tagModelLoader);

    await Tag.xDelete({
      id: tagId
    });
  }

  async updateTagById(tagId: string, tagData: any): Promise<any> {
    const Tag = this.getModelByLoader(tagModelLoader);
    return await Tag.xUpdate({
      id: tagId
    }, tagData);
  }

  async getAllTags(query?: any): Promise<any> {
    const Tag = this.getModelByLoader(tagModelLoader);
    return await Tag.xGetAll(query);
  }
}
