import * as uuid from 'uuid';

import {
  defaultThroughput,
  IDbModelLoaderMap,
  JsonApi
} from '@vifros/serverless-json-api';

import { userModelLoader } from './user';

export const postModelLoader: IDbModelLoaderMap = {
  id               : 'post',
  tableName        : process.env.SERVICE_DYNAMODB_TABLE_POSTS,
  schema           : {
    id       : {
      type    : String,
      hashKey : true,
      readonly: true,
      default : () => {
        return uuid.v4();
      }
    },
    type     : {
      type        : String,
      readonly    : true,
      default     : 'post',
      forceDefault: true,
      index       : [
        {
          global    : true,
          name      : 'typeCreatedAtGlobalIndex',
          rangeKey  : 'createdAt',
          project   : true,
          throughput: defaultThroughput
        },
        {
          global    : true,
          name      : 'typeTitleGlobalIndex',
          rangeKey  : 'title',
          project   : true,
          throughput: defaultThroughput
        },
        {
          global    : true,
          name      : 'typeAuthorIdGlobalIndex',
          rangeKey  : 'authorId',
          project   : true,
          throughput: defaultThroughput
        }
      ]
    },
    title    : {
      type    : String,
      required: true
    },
    authorId : {
      type       : String,
      required   : true,
      validations: {
        isUUID  : [4],
        itExists: async function (value, db): Promise<boolean | string> {
          const User = db.getModelByLoader(userModelLoader);

          let user = await User.xGet({
            id: value
          });

          return !!user;
        }
      }
    },
    tags     : {
      type: 'list',
      list: [
        {
          type: String
        }
      ]
    },
    createdAt: {
      type: Number
    },
    updatedAt: {
      type: Number
    }
  },
  schemaOptions    : {
    throughput: defaultThroughput
  },
  serializerOptions: {
    links: {
      self: function (data) {
        let extras: any = JsonApi.getExtras(data);

        let url = '/v1';

        return (extras && extras.isNested)
               ? `${url}/users/${data.authorId}/posts/${data.id}`
               : `${url}/posts/${data.id}`;
      }
    }
  }
};
