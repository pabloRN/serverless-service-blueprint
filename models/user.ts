import * as uuid from 'uuid';

import {
  defaultThroughput,
  IDbModelLoaderMap
} from '@vifros/serverless-json-api';

export const userModelLoader: IDbModelLoaderMap = {
  id               : 'user',
  tableName        : process.env.SERVICE_DYNAMODB_TABLE_USERS,
  schema           : {
    id       : {
      type    : String,
      hashKey : true,
      readonly: true,
      default : () => {
        return uuid.v4();
      }
    },
    type     : {
      type        : String,
      readonly    : true,
      default     : 'user',
      forceDefault: true,
      index       : [
        {
          global    : true,
          name      : 'typeCreatedAtGlobalIndex',
          rangeKey  : 'createdAt',
          project   : true,
          throughput: defaultThroughput
        },
        {
          global    : true,
          name      : 'typeUsernameGlobalIndex',
          rangeKey  : 'username',
          project   : true,
          throughput: defaultThroughput
        }
      ]
    },
    isAdmin  : {
      type   : Boolean,
      default: false
    },
    username : {
      type    : String,
      required: true
    },
    address  : {
      type: 'map',
      map : {
        streetAddress      : {
          type    : String,
          required: true
        },
        state              : {
          type    : String,
          required: true
        },
        country            : {
          type    : String,
          required: true
        },
        postalCode         : {
          type    : Number,
          required: true
        },
        postOfficeBoxNumber: {
          type: Number
        }
      }
    },
    createdAt: {
      type: Number
    },
    updatedAt: {
      type: Number
    }
  },
  schemaOptions    : {
    throughput: defaultThroughput
  },
  serializerOptions: {
    links: {
      self: function (data) {
        return '/v1/users/' + data.id;
      }
    }
  }
};
